package main

import (
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/lrossi-dev/scs-api/internal/http"
	"gitlab.com/lrossi-dev/scs-api/internal/http/actions"

	_ "github.com/lib/pq"
)

func main() {
	connString := "postgres://scsapi:scspass@localhost:5432/scsdb?sslmode=disable"
	db, err := sql.Open("postgres", connString)

	if err != nil {
		log.Fatal(err)
	}

	if err = db.Ping(); err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	g := gin.Default()
	actions.Configure(db)
	http.SetRoutes(g)

	g.Run(":8080")
}
