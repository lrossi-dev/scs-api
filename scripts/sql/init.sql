CREATE TABLE IF NOT EXISTS Project (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    Path VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS Scan (
    ID SERIAL PRIMARY KEY,
    Status VARCHAR(20),
    Creation_date TIMESTAMP NOT NULL,
    Project_id INTEGER REFERENCES Project
);


CREATE TABLE IF NOT EXISTS Rule (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    Extensions VARCHAR(255),
    Regexp VARCHAR(255) NOT NULL,
    Description VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS Violation (
    ID SERIAL PRIMARY KEY,
    Filename VARCHAR(255) NOT NULL,
    Line INTEGER NOT NULL,
    Rule_id INTEGER REFERENCES Rule,
    Scan_id INTEGER REFERENCES Scan
);

INSERT INTO Rule(Name, Extensions, Regexp, Description) VALUES
('Cross Site Scripting', '^.*\\.(html|js)$', '(alert)', 'Alerts should be avoided'),
('SQL Injection', '', '^.*\".*(SELECT|WHERE|\%s).*\".*$', 'You are open to SQL Injections');


