# scs-api

Backend for the Source Code Scanner project

## How to get started?

Install the dependencies by running `go get .`, then start the API by running `cd cmd/api && go run .`.

This project requires a postgres database to be listening on port 5432, so I recommend running:

```
$ docker run --name scs-db \
    -e POSTGRES_PASSWORD=scspass \
    -e POSTGRES_USER=scsapi \
    -e POSTGRES_DB=scsdb \
    -p 5432:5432 \
    -v $(pwd)/scripts/sql/:/docker-entrypoint-initdb.d \
    -d --rm postgres
```

The variables used by the app are arealdy set to the correct values in above command.

## How to use it?

You can either play around with the [Postman Collection](https://gitlab.com/lrossi-dev/scs-api/-/tree/main/scripts/scs-api.postman_collection.json) or use the API as backend for [scs-web](https://gitlab.com/lrossi-dev/scs-web).

By default, only two rules are created: **SQL Injection** rule and **Cross Site Scripting** rule. If you want to create new ones, take a look at the Postman collection. The REST API is the only way to create new ones.

Regarding *projects*, the path specified should either be relative to the running directory (e.g.: `$(pwd)/cmd/api`) or an absolute path. There is a [files](ttps://gitlab.com/lrossi-dev/scs-api/-/tree/main/files) directory containing a few files that could be used for testing.
