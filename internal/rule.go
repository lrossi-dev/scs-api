package internal

type Rule struct {
	Id          int    `json:"id"`
	Regexp      string `json:"regexp"`
	Extensions  string `json:"extensions"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
