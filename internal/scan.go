package internal

import "time"

type Scan struct {
	Id         int          `json:"id"`
	Status     string       `json:"status"`
	Violations []*Violation `json:"violations"`
	CreatedAt  time.Time    `json:"date"`
}
