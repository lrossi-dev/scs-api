package scan

import (
	"bufio"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"time"

	"gitlab.com/lrossi-dev/scs-api/internal"
	"gitlab.com/lrossi-dev/scs-api/internal/rule"
	"gitlab.com/lrossi-dev/scs-api/internal/violation"
)

type ScanService struct {
	ScanRepository      *ScanRepository
	ViolationRepository *violation.ViolationRepository
	RuleRepository      *rule.RuleRepository
}

func (scanService *ScanService) ScanProject(project internal.Project) (int, error) {
	status := "RUNNING"
	id, err := scanService.CreateScan(project, status)
	if err != nil {
		log.Printf("[ScanService] Error while scanning project: project=[id=%d,name=%s, path=%s], error=%s", project.Id, project.Name, project.Path, err)
		return id, err
	}

	rules, err := scanService.RuleRepository.GetAllRules()
	if err != nil {
		log.Printf("[ScanService] Error while scanning project: project=[id=%d,name=%s, path=%s], error=%s", project.Id, project.Name, project.Path, err)
		return id, err
	}

	err = filepath.Walk(project.Path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Printf("[ScanService] Error while listing files: error=%s", err)
			return err
		}
		if !info.IsDir() {
			log.Printf("[ScanService] File found: file=%s", path)
			scanService.CheckFile(id, path, &rules, &status)
		}
		return nil
	})
	if err != nil {
		log.Printf("[ScanService] Error while scanning project: project=[id=%d,name=%s,path=%s], error=%s",
			project.Id, project.Name, project.Path, err)
		return id, err
	}
	if status == "RUNNING" {
		id, _ = scanService.UpdateScanStatus(id, "SUCCESS")
	}

	return id, nil
}

func (scanService *ScanService) GetAllScansByProjectId(projectId int) ([]*internal.Scan, error) {
	return scanService.ScanRepository.GetAllScansByProjectId(projectId)
}

func (scanService *ScanService) GetScanById(scanId int) (internal.Scan, error) {
	scan, err := scanService.ScanRepository.GetScanById(scanId)
	if err != nil {
		log.Printf("[ScanService] Error while fetching scan data: scanId=%d, error=%s", scanId, err)
		return scan, err
	}
	violations, err := scanService.ViolationRepository.GetViolationsByScanID(scanId)
	if err != nil {
		log.Printf("[ScanService] Error while fetching violations")
		return scan, err
	}
	scan.Violations = violations
	return scan, nil
}

func (scanService *ScanService) CreateScan(project internal.Project, status string) (int, error) {
	scan := internal.Scan{
		Status:    status,
		CreatedAt: time.Now(),
	}

	return scanService.ScanRepository.Insert(scan, project.Id)
}

func (scanService *ScanService) UpdateScanStatus(scanId int, status string) (int, error) {
	scan := internal.Scan{
		Id:     scanId,
		Status: status,
	}
	return scanService.ScanRepository.Update(scan)
}

func (scanService *ScanService) CreateViolation(id int, rule *internal.Rule, fileName string, line int) (int, error) {
	violation := internal.Violation{
		File: fileName,
		Line: line,
		Rule: *rule,
	}
	return scanService.ViolationRepository.Insert(violation, id)
}

func (scanService *ScanService) CheckFile(id int, path string, rules *[]*internal.Rule, status *string) {
	for _, rule := range *rules {
		var e error
		matches := len(rule.Extensions) == 0
		if !matches {
			matches, e = regexp.MatchString(rule.Extensions, path)
		}
		if e == nil && matches {
			file, err := os.Open(path)
			if err != nil {
				log.Printf("[ScanService] Unable to open file: file=%s error=%s", path, err)
				continue
			}
			defer file.Close()
			scanner := bufio.NewScanner(file)
			lineCounter := 0
			for scanner.Scan() {
				lineCounter++
				regex, err := regexp.Compile(rule.Regexp)
				if err != nil {
					log.Printf("[ScanService] Error compiling regex: regex=%s, err=%s", regex, err)
					continue
				}
				if regex.MatchString(scanner.Text()) {
					violation_id, err := scanService.CreateViolation(id, rule, path, lineCounter)
					if err != nil {
						log.Printf("[ScanService] Error while creating violation: error=%s", err)
						continue
					} else {
						log.Printf("[ScanService] Violation created: violation_id=%d", violation_id)
						if *status != "FAILED" {
							*status = "FAILED"
							scanService.UpdateScanStatus(id, *status)
						}
					}

				}
			}
		} else {
			log.Printf("[ScanService] Rule didn't match. file=%s, rule=[id=%d,name=%s,extensions=%s,regex=%s]", path, rule.Id, rule.Name, rule.Extensions, rule.Regexp)
		}
	}
}
