package scan

import (
	"database/sql"
	"log"
	"time"

	"gitlab.com/lrossi-dev/scs-api/internal"
)

type ScanRepository struct {
	Db *sql.DB
}

func (sr *ScanRepository) Insert(scan internal.Scan, project_id int) (int, error) {
	query := `INSERT INTO Scan(Status, Creation_date, Project_id) VALUES ($1, $2, $3) RETURNING ID`

	var pk int
	err := sr.Db.QueryRow(query, scan.Status, scan.CreatedAt, project_id).Scan(&pk)
	if err != nil {
		log.Printf("Error while inserting to database: scan=[status=%s,creation_date=%s,project_id=%d], error=%s", scan.Status, scan.CreatedAt, project_id, err)
		return pk, err
	}
	return pk, nil
}

func (sr *ScanRepository) GetScanById(scanId int) (internal.Scan, error) {
	query := `SELECT ID, Status, Creation_date FROM Scan WHERE ID = $1`

	var id int
	var status string
	var createdAt time.Time

	var scan internal.Scan

	err := sr.Db.QueryRow(query, scanId).Scan(&id, &status, &createdAt)
	if err != nil {
		log.Printf("Error while fetching data: scanId=%d, error=%s", scanId, err)
		return scan, err
	}

	scan = internal.Scan{
		Id:        id,
		Status:    status,
		CreatedAt: createdAt,
	}
	return scan, nil
}

func (sr *ScanRepository) GetAllScansByProjectId(project_id int) ([]*internal.Scan, error) {
	query := `SELECT ID, Status, Creation_date FROM Scan WHERE Project_id = $1`

	rows, err := sr.Db.Query(query, project_id)
	if err != nil {
		log.Printf("Error while fetching scans in database: project_id=%d, error=%s", project_id, err)
		return nil, err
	}

	var items []*internal.Scan

	defer rows.Close()

	for rows.Next() {
		var id int
		var created_at time.Time
		var status string

		err := rows.Scan(&id, &status, &created_at)
		if err != nil {
			log.Printf("Error while mapping data to entity: error=%s", err)
			return nil, err
		}

		scan := &internal.Scan{
			Id:        id,
			Status:    status,
			CreatedAt: created_at,
		}

		items = append(items, scan)
	}
	return items, nil
}

func (sr *ScanRepository) Update(scan internal.Scan) (int, error) {
	query := `UPDATE Scan SET Status = $2 WHERE ID = $1 RETURNING ID`

	var pk int
	err := sr.Db.QueryRow(query, scan.Id, scan.Status).Scan(&pk)
	if err != nil {
		log.Printf("Error while updating entry: scan=[id=%d,status=%s], error=%s", scan.Id, scan.Status, err)
		return pk, err
	}
	return pk, nil
}
