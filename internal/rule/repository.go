package rule

import (
	"database/sql"
	"log"

	"gitlab.com/lrossi-dev/scs-api/internal"
)

type RuleRepository struct {
	Db *sql.DB
}

func (rr *RuleRepository) Insert(rule internal.Rule) (int, error) {
	query := `INSERT INTO Rule(Name, Description, Regexp) VALUES ($1, $2, $3) RETURNING ID`

	var pk int
	err := rr.Db.QueryRow(query, rule.Name, rule.Description, rule.Regexp).Scan(&pk)
	if err != nil {
		log.Printf("Error while inserting to database: rule=[name=%s,description=%s,regex=%s], error=%s", rule.Name, rule.Description, rule.Regexp, err)
		return pk, err
	}
	return pk, nil
}

func (rr *RuleRepository) GetAllRules() ([]*internal.Rule, error) {
	query := `SELECT ID, Name, Description, Extensions, Regexp FROM Rule`

	rows, err := rr.Db.Query(query)
	if err != nil {
		log.Printf("Error while fetching in database: error=%s", err)
		return nil, err
	}

	var items []*internal.Rule

	defer rows.Close()

	for rows.Next() {
		var id sql.NullInt32
		var name, description, extensions, regex sql.NullString

		err := rows.Scan(&id, &name, &description, &extensions, &regex)
		if err != nil {
			log.Printf("Error while mapping data to entity: rule=[id=%d,name=%s,description=%s,regex=%s], error=%s", id, name, description, regex, err)
			return nil, err
		}

		item := &internal.Rule{
			Id:          int(id.Int32),
			Name:        name.String,
			Description: description.String,
			Extensions:  extensions.String,
			Regexp:      regex.String,
		}

		items = append(items, item)
	}

	return items, nil
}
