package violation

import (
	"database/sql"
	"log"

	"gitlab.com/lrossi-dev/scs-api/internal"
)

type ViolationRepository struct {
	Db *sql.DB
}

func (vr *ViolationRepository) Insert(violation internal.Violation, scan_id int) (int, error) {
	query := `INSERT INTO Violation(Filename, Line, Rule_id, Scan_id) VALUES ($1, $2, $3, $4) RETURNING ID`

	var pk int
	err := vr.Db.QueryRow(query, violation.File, violation.Line, violation.Rule.Id, scan_id).Scan(&pk)
	if err != nil {
		log.Printf("Error while inserting to database: violation=[file=%s,line=%d], rule_id=%d, scan_id=%d, error=%s", violation.File, violation.Line, violation.Rule.Id, scan_id, err)
		return pk, err
	}
	return pk, nil
}

func (vr *ViolationRepository) GetViolationsByScanID(scan_id int) ([]*internal.Violation, error) {
	query := `SELECT v.ID, v.Filename, v.Line, r.ID, r.Name, r.Description, r.Regexp
				FROM Violation v
				INNER JOIN Rule r
				ON v.Rule_id = r.ID
				WHERE v.Scan_id = $1`

	rows, err := vr.Db.Query(query, scan_id)
	if err != nil {
		log.Printf("Error while fetching in database: error=%s", err)
		return nil, err
	}

	var items []*internal.Violation

	defer rows.Close()

	for rows.Next() {
		var violation_id, violation_line, rule_id int
		var violation_file, rule_name, rule_description, rule_regex string

		err := rows.Scan(&violation_id, &violation_file, &violation_line, &rule_id, &rule_name, &rule_description, &rule_regex)
		if err != nil {
			log.Printf("Error while mapping data to entity: error=%s", err)
			return nil, err
		}

		rule := &internal.Rule{
			Id:          rule_id,
			Name:        rule_name,
			Description: rule_description,
			Regexp:      rule_regex,
		}

		violation := &internal.Violation{
			Id:   violation_id,
			File: violation_file,
			Line: violation_line,
			Rule: *rule,
		}

		items = append(items, violation)
	}

	return items, nil
}
