package actions

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/lrossi-dev/scs-api/internal"
	"gitlab.com/lrossi-dev/scs-api/internal/project"
)

var projectRepository project.ProjectRepository

func ConfigureProjectActions(db *sql.DB) {
	projectRepository = project.ProjectRepository{
		Db: db,
	}
}

func PostProject(ctx *gin.Context) {
	var project internal.Project

	if err := ctx.BindJSON(&project); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	id := projectRepository.Insert(project)
	ctx.Header("Location", fmt.Sprintf("%s%d", "/projects/", id))
	ctx.JSON(http.StatusCreated, nil)
}

func GetAllProjects(ctx *gin.Context) {
	projects := projectRepository.GetAllProjects()
	ctx.JSON(http.StatusOK, projects)
}
