package actions

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/lrossi-dev/scs-api/internal"
	"gitlab.com/lrossi-dev/scs-api/internal/rule"
)

var ruleRepository rule.RuleRepository

func ConfigureRuleActions(db *sql.DB) {
	ruleRepository = rule.RuleRepository{
		Db: db,
	}
}

func PostRule(ctx *gin.Context) {
	var rule internal.Rule

	if err := ctx.BindJSON(&rule); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	id, err := ruleRepository.Insert(rule)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.Header("Location", fmt.Sprintf("/rules/%d", id))
	ctx.JSON(http.StatusCreated, nil)
}

func GetAllRules(ctx *gin.Context) {
	rules, err := ruleRepository.GetAllRules()
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, rules)
}
