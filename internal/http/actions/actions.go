package actions

import "database/sql"

func Configure(db *sql.DB) {
	ConfigureRuleActions(db)
	ConfigureProjectActions(db)
	ConfigureScanActions(db)
}
