package actions

import (
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/lrossi-dev/scs-api/internal"
	"gitlab.com/lrossi-dev/scs-api/internal/rule"
	"gitlab.com/lrossi-dev/scs-api/internal/scan"
	"gitlab.com/lrossi-dev/scs-api/internal/violation"
)

var scanService scan.ScanService

func ConfigureScanActions(db *sql.DB) {
	scanService = scan.ScanService{
		ScanRepository: &scan.ScanRepository{
			Db: db,
		},
		RuleRepository: &rule.RuleRepository{
			Db: db,
		},
		ViolationRepository: &violation.ViolationRepository{
			Db: db,
		},
	}
}

func ScanProject(ctx *gin.Context) {
	var project internal.Project
	if err := ctx.BindJSON(&project); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	id, err := scanService.ScanProject(project)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.Header("Location", fmt.Sprintf("/scans/%d", id))
	ctx.JSON(http.StatusCreated, nil)
}

func GetAllScansByProjectId(ctx *gin.Context) {
	projectId, err := strconv.Atoi(ctx.Param("projectId"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	scans, err := scanService.GetAllScansByProjectId(projectId)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	if len(scans) == 0 {
		ctx.JSON(http.StatusNotFound, http.NoBody)
		return
	}
	ctx.JSON(http.StatusOK, scans)

}

func GetScanById(ctx *gin.Context) {
	scanId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	scan, err := scanService.GetScanById(scanId)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, scan)
}
