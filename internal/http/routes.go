package http

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/lrossi-dev/scs-api/internal/http/actions"
)

func SetRoutes(g *gin.Engine) {
	g.Use(CORSMiddleware())
	g.GET("/projects", actions.GetAllProjects)
	g.POST("/projects", actions.PostProject)

	g.GET("/rules", actions.GetAllRules)
	g.POST("/rules", actions.PostRule)

	g.POST("/scans", actions.ScanProject)
	g.GET("/projects/:projectId/scans", actions.GetAllScansByProjectId)
	g.GET("/scans/:id", actions.GetScanById)
}
