package internal

type Project struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Path  string `json:"path"`
	Scans []Scan `json:"scans"`
}
