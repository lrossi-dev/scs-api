package project

import (
	"database/sql"
	"log"

	"gitlab.com/lrossi-dev/scs-api/internal"
)

type ProjectRepository struct {
	Db *sql.DB
}

func (pr *ProjectRepository) Insert(project internal.Project) int {
	query := `INSERT INTO Project(Name, Path) VALUES ($1, $2) RETURNING ID`

	var pk int
	err := pr.Db.QueryRow(query, project.Name, project.Path).Scan(&pk)
	if err != nil {
		log.Fatal(err)
	}
	return pk
}

func (pr *ProjectRepository) GetAllProjects() []*internal.Project {
	query := `SELECT * FROM PROJECT`

	rows, err := pr.Db.Query(query)
	if err != nil {
		log.Fatal(err)
	}

	var items []*internal.Project

	defer rows.Close()

	for rows.Next() {
		var id int
		var name, path string

		err := rows.Scan(&id, &name, &path)
		if err != nil {
			log.Fatal(err)
		}

		item := &internal.Project{
			Id:   id,
			Name: name,
			Path: path,
		}

		items = append(items, item)
	}

	return items
}
