package internal

type Violation struct {
	Id   int    `json:"id"`
	File string `json:"file"`
	Line int    `json:"line"`
	Rule Rule   `json:"rule"`
}
